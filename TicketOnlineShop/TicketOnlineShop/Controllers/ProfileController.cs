﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TicketOnlineShop.Models;
using TicketOnlineShop.Models.Abstractions;

namespace TicketOnlineShop.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private ITicketService _ticketService;
        private IOrderService _orderService;
        private UserManager<MyUser> _userManager;

        public ProfileController(ITicketService ticketService, IOrderService orderService
            , UserManager<MyUser> userManager)
        {
            _ticketService = ticketService;
            _orderService = orderService;
            _userManager = userManager;
        }

        public ActionResult Index()
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            return View(user);
        }

        public ActionResult MyTickets()
        {
            return View(_ticketService.GetAllTickets());
        }

        public ActionResult MyOrders()
        {            
            return View(_orderService.GetAllOrders());
        }
    }
}
