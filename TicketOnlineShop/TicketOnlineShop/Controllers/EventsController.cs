﻿using Microsoft.AspNetCore.Mvc;
using TicketOnlineShop.Models.Abstractions;

namespace TicketOnlineShop.Controllers
{
    public class EventsController : Controller
    {
        private IEventService _eventService;
        private ITicketService _ticketService;

        public EventsController(IEventService eventService, ITicketService ticketService)
        {
            _eventService = eventService;
            _ticketService = ticketService;
        }

        public IActionResult Index()
        {   
            return View(_eventService.GetAllEvents());
        }

        [HttpGet]
        public IActionResult AvailableTickets(string eventName)
        {
            var currentTickets = _ticketService.GetTicktesByEventName(eventName);
            
            return View(currentTickets);
        }
    }
}
