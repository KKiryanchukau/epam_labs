﻿namespace TicketOnlineShop.Models.Entities
{
    public class Order
    {        
        public int Id { get; set; }
        public Ticket Ticket { get; set; }
        public MyUser Buyer { get; set; }
        public OrderStatus Status { get; set; }
    }
}
