﻿using System;

namespace TicketOnlineShop.Models.Entities
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public Venue Venue { get; set; }
        public string PathToBanner { get; set; }
        public string Description { get; set; }
    }
}
