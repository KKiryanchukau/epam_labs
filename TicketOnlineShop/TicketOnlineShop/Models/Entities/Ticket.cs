﻿namespace TicketOnlineShop.Models.Entities
{
    public class Ticket
    {
        public int Id { get; set; }
        public Event Event { get; set; }
        public decimal Price { get; set; }
        public MyUser Seller { get; set; }
        public TicketStatus Status { get; set; }
    }
}
