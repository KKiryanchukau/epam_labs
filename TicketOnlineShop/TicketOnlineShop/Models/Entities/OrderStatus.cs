﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketOnlineShop.Models.Entities
{
    public enum OrderStatus
    {
        WaitingForConfirmation,
        Confirmed,
        Rejected
    }
}
