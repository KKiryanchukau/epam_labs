﻿namespace TicketOnlineShop.Models.Entities
{
    public enum TicketStatus
    {
        Selling,
        WaitingConfirmation,
        Sold
    }
}
