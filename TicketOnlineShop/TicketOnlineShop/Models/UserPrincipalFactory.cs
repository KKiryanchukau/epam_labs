﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Threading.Tasks;
using TicketOnlineShop.Models;

namespace TicketOnlineShop
{
    public class UserPrincipalFactory : IUserClaimsPrincipalFactory<MyUser>
    {
        private readonly IdentityOptions options;

        public UserPrincipalFactory(IOptions<IdentityOptions> optionsAccessor)
        {
            options = optionsAccessor?.Value ?? new IdentityOptions();
        }

        public Task<ClaimsPrincipal> CreateAsync(MyUser user)
        {
            var identity = new ClaimsIdentity(
                options.Cookies.ApplicationCookieAuthenticationScheme,
                options.ClaimsIdentity.UserNameClaimType,
                options.ClaimsIdentity.RoleClaimType);
            
            identity.AddClaim(new Claim(options.ClaimsIdentity.UserNameClaimType, user.FirstName));
            identity.AddClaim(new Claim(options.ClaimsIdentity.RoleClaimType, user.Role.RoleName));

            var principal = new ClaimsPrincipal(identity);

            return Task.FromResult(principal);
        }
    }
}
