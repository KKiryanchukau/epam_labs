﻿using System;
using System.Collections.Generic;
using TicketOnlineShop.Models.Abstractions;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Implementations
{
    public class TicketRepository : ITicketRepository
    {
        private List<Ticket> _tickets = new List<Ticket>()
        {
            new Ticket
                {
                    Event = new Event { Name = "Bestival", Date = DateTime.Parse("25-07-2017"), Description = "The wonderful world of Bestival was born out of ten fun years of pioneering music events and record releases from Rob da Bank’s Sunday Best empire. The BBC Radio 1 leftfield DJ had a dream to one day create his vision of how the modern day festival should be. Along with Creative Director and wife Josie da Bank and co founders / partners John and Ziggy from Get Involved that dream is now fulfilled. On a mission to change the face of independent festival culture, the fearless foursome are bringing some magic to the shores of the Solent and spreading the love worldwide.",
                    Venue = new Venue { Address = "Lulworth Estate", City = new City { Name = "Bournemouth" } },
                    PathToBanner = @"\images\Bestival.jpg" },
                    Seller = new MyUser { FirstName = "Daniel", LastName = "Brown" },
                    Price = 23,
                    Status = TicketStatus.Selling
                },
                new Ticket
                {
                    Event = new Event { Name = "Donauinselfest Festival", Date = DateTime.Parse("05-08-2017"), Description = "The Donauinselfest (German for Danube Island Festival) is an open-air free music festival taking place annually at Donauinsel in Vienna, Austria. It usually takes place in mid or end of June. According to numerous media reports the Donauinselfest is Europe's biggest open air event; soon expected to reach the 3 million visitors mark. Some 1500 volunteer assistants and visible police presence help make the Donauinselfest one of the most secure big events. ",
                    Venue = new Venue { Address = "Donauinsel", City = new City { Name = "Wien" } },
                    PathToBanner = @"\images\DonauinselfestFestival.jpg" },
                    Seller = new MyUser { FirstName = "Alfie", LastName = "Taylor" },
                    Price = 56,
                    Status = TicketStatus.Sold
                },
                new Ticket
                {
                    Event = new Event { Name = "EarthGarden", Date = DateTime.Parse("31-10-2017"), Description = "Situated on the island of Malta, Earth Garden is definitely one of Europe’s best kept secrets.  After celebrating its 10-year anniversary, attracting over 20,000 visitors, the festival is now officially being launched internationally.  Retaining the green footprint & eclectic music styles, it offers 4 music areas with over 35 live acts, 35 DJs, 2 shaded camping zones with all amenities, Jamming area, Healing fields, Ethnic market & holistic area, Fun park, food court with a variety of healthy & diverse food & numerous bars. This is the festival that kicks off your summer which guarantees loads of fun, unbeatable prices with the best weather conditions in one of Europe’s top destinations.",
                    Venue = new Venue { Address = "Ta' Qali National Park", City = new City { Name = "Attard" } },
                    PathToBanner = @"\images\EarthGarden.jpg" },
                    Seller = new MyUser { FirstName = "Thomas", LastName = "Evans" },
                    Price = 45,
                    Status = TicketStatus.WaitingConfirmation
                },
                new Ticket
                {
                    Event = new Event { Name = "Tomorrowland", Date = DateTime.Parse("15.07.2017"), Description = "Tomorrowland is a large electronic music festival held in Belgium. It used to be organized as a joint venture by the original founders, the brothers Beers. The festival takes place in the town of Boom, Belgium (16 kilometers (10 miles) south of Antwerp, 32 kilometers (20 miles) north of Brussels), and has been organized since 2005. It has since become one of the most notable global music festivals.",
                    Venue = new Venue { Address = "PRC de Schorre", City = new City { Name = "Boom" } },
                    PathToBanner = @"\images\Tomorrowland.jpg" },
                    Seller = new MyUser { FirstName = "James", LastName = "Williams" },
                    Price = 70,
                    Status = TicketStatus.WaitingConfirmation
                },
                new Ticket
                {
                    Event = new Event { Name = "Bestival", Date = DateTime.Parse("25-07-2017"), Description = "The wonderful world of Bestival was born out of ten fun years of pioneering music events and record releases from Rob da Bank’s Sunday Best empire. The BBC Radio 1 leftfield DJ had a dream to one day create his vision of how the modern day festival should be. Along with Creative Director and wife Josie da Bank and co founders / partners John and Ziggy from Get Involved that dream is now fulfilled. On a mission to change the face of independent festival culture, the fearless foursome are bringing some magic to the shores of the Solent and spreading the love worldwide.",
                    Venue = new Venue { Address = "Lulworth Estate", City = new City { Name = "Bournemouth" } },
                    PathToBanner = @"\images\Bestival.jpg" },
                    Seller = new MyUser { FirstName = "Joshua", LastName = "Wilson" },
                    Price = 20,
                    Status = TicketStatus.Selling
                },
                new Ticket
                {
                    Event = new Event { Name = "Donauinselfest Festival", Date = DateTime.Parse("05-08-2017"), Description = "The Donauinselfest (German for Danube Island Festival) is an open-air free music festival taking place annually at Donauinsel in Vienna, Austria. It usually takes place in mid or end of June. According to numerous media reports the Donauinselfest is Europe's biggest open air event; soon expected to reach the 3 million visitors mark. Some 1500 volunteer assistants and visible police presence help make the Donauinselfest one of the most secure big events. ",
                    Venue = new Venue { Address = "Ta' Qali National Park", City = new City { Name = "Wien" } },
                    PathToBanner = @"\images\DonauinselfestFestival.jpg" },
                    Seller = new MyUser { FirstName = "Jacob", LastName = "Jones" },
                    Price = 50,
                    Status = TicketStatus.Selling
                },
                 new Ticket
                {
                    Event = new Event { Name = "EarthGarden", Date = DateTime.Parse("31-10-2017"), Description = "Situated on the island of Malta, Earth Garden is definitely one of Europe’s best kept secrets.  After celebrating its 10-year anniversary, attracting over 20,000 visitors, the festival is now officially being launched internationally.  Retaining the green footprint & eclectic music styles, it offers 4 music areas with over 35 live acts, 35 DJs, 2 shaded camping zones with all amenities, Jamming area, Healing fields, Ethnic market & holistic area, Fun park, food court with a variety of healthy & diverse food & numerous bars. This is the festival that kicks off your summer which guarantees loads of fun, unbeatable prices with the best weather conditions in one of Europe’s top destinations.",
                    Venue = new Venue { Address = "Aerobaha", City = new City { Name = "Attard" } },
                    PathToBanner = @"\images\EarthGarden.jpg" },
                    Seller = new MyUser { FirstName = "Alexander", LastName = "Miller" },
                    Price = 48,
                    Status = TicketStatus.Sold
                },
                 new Ticket
                {
                    Event = new Event { Name = "Tomorrowland", Date = DateTime.Parse("15.07.2017"), Description = "Tomorrowland is a large electronic music festival held in Belgium. It used to be organized as a joint venture by the original founders, the brothers Beers. The festival takes place in the town of Boom, Belgium (16 kilometers (10 miles) south of Antwerp, 32 kilometers (20 miles) north of Brussels), and has been organized since 2005. It has since become one of the most notable global music festivals.",
                    Venue = new Venue { Address = "PRC de Schorre", City = new City { Name = "Boom" } },
                    PathToBanner = @"\images\Tomorrowland.jpg" },
                    Seller = new MyUser { FirstName = "Christopher", LastName = "Moore" },
                    Price = 72,
                    Status = TicketStatus.Selling
                },
                 new Ticket
                {
                    Event = new Event { Name = "Bestival", Date = DateTime.Parse("25-07-2017"), Description = "The wonderful world of Bestival was born out of ten fun years of pioneering music events and record releases from Rob da Bank’s Sunday Best empire. The BBC Radio 1 leftfield DJ had a dream to one day create his vision of how the modern day festival should be. Along with Creative Director and wife Josie da Bank and co founders / partners John and Ziggy from Get Involved that dream is now fulfilled. On a mission to change the face of independent festival culture, the fearless foursome are bringing some magic to the shores of the Solent and spreading the love worldwide.",
                    Venue = new Venue { Address = "Lulworth Estate", City = new City { Name = "Bournemouth" } },
                    PathToBanner = @"\images\Bestival.jpg" },
                    Seller = new MyUser { FirstName = "Antony", LastName = "Taylor" },
                    Price = 15,
                    Status = TicketStatus.WaitingConfirmation
                },
                 new Ticket
                {
                    Event = new Event { Name = "Bestival", Date = DateTime.Parse("05-08-2017"), Description = "The wonderful world of Bestival was born out of ten fun years of pioneering music events and record releases from Rob da Bank’s Sunday Best empire. The BBC Radio 1 leftfield DJ had a dream to one day create his vision of how the modern day festival should be. Along with Creative Director and wife Josie da Bank and co founders / partners John and Ziggy from Get Involved that dream is now fulfilled. On a mission to change the face of independent festival culture, the fearless foursome are bringing some magic to the shores of the Solent and spreading the love worldwide.",
                    Venue = new Venue { Address = "Lulworth Estate", City = new City { Name = "Bournemouth" } },
                    PathToBanner = @"\images\Bestival.jpg" },
                    Seller = new MyUser { FirstName = "Harry", LastName = "Johnson" },
                    Price = 27,
                    Status = TicketStatus.Sold
                },
                 new Ticket
                {
                    Event = new Event { Name = "Donauinselfest Festival", Date = DateTime.Parse("31-10-2017"), Description = "The Donauinselfest (German for Danube Island Festival) is an open-air free music festival taking place annually at Donauinsel in Vienna, Austria. It usually takes place in mid or end of June. According to numerous media reports the Donauinselfest is Europe's biggest open air event; soon expected to reach the 3 million visitors mark. Some 1500 volunteer assistants and visible police presence help make the Donauinselfest one of the most secure big events. ",
                    Venue = new Venue { Address = "Ta' Qali National Park", City = new City { Name = "Wien" } },
                    PathToBanner = @"\images\DonauinselfestFestival.jpg" },
                    Seller = new MyUser { FirstName = "Olivia", LastName = "Wilson" },
                    Price = 55,
                    Status = TicketStatus.Sold
                },
                 new Ticket
                {
                    Event = new Event { Name = "EarthGarden", Date = DateTime.Parse("15.07.2017"), Description = "Situated on the island of Malta, Earth Garden is definitely one of Europe’s best kept secrets.  After celebrating its 10-year anniversary, attracting over 20,000 visitors, the festival is now officially being launched internationally.  Retaining the green footprint & eclectic music styles, it offers 4 music areas with over 35 live acts, 35 DJs, 2 shaded camping zones with all amenities, Jamming area, Healing fields, Ethnic market & holistic area, Fun park, food court with a variety of healthy & diverse food & numerous bars. This is the festival that kicks off your summer which guarantees loads of fun, unbeatable prices with the best weather conditions in one of Europe’s top destinations.",
                    Venue = new Venue { Address = "Aerobaha", City = new City { Name = "Attard" } },
                    PathToBanner = @"\images\EarthGarden.jpg" },
                    Seller = new MyUser { FirstName = "Sophie", LastName = "Smith" },
                    Price = 44,
                    Status = TicketStatus.Selling
                },
                 new Ticket
                {
                    Event = new Event { Name = "Tomorrowland", Date = DateTime.Parse("15.07.2017"), Description = "Tomorrowland is a large electronic music festival held in Belgium. It used to be organized as a joint venture by the original founders, the brothers Beers. The festival takes place in the town of Boom, Belgium (16 kilometers (10 miles) south of Antwerp, 32 kilometers (20 miles) north of Brussels), and has been organized since 2005. It has since become one of the most notable global music festivals.",
                    Venue = new Venue { Address = "PRC de Schorre", City = new City { Name = "Boom" } },
                    PathToBanner = @"\images\Tomorrowland.jpg" },
                    Seller = new MyUser { FirstName = "Elizabeth", LastName = "Miller" },
                    Price = 99,
                    Status = TicketStatus.Sold
                }
        };

        public IEnumerable<Ticket> Tickets
        {
            get
            {
                return _tickets;
            }
        }
    }
}
