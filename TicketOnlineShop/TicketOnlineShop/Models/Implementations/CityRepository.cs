﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Abstractions;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Implementations
{
    public class CityRepository : ICityRepository
    {
        private List<City> _cities = new List<City>()
        {
            new City { Name = "Bournemouth" },
            new City { Name = "Wien" },
            new City { Name = "Attard" },
            new City { Name = "Boom" },
            new City { Name = "Porto" },
            new City { Name = "Budapest" }
        };

        public IEnumerable<City> Cities
        {
            get
            {
                return _cities;
            }
        }
    }
}
