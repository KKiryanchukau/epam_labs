﻿using System.Collections.Generic;
using System.Linq;
using TicketOnlineShop.Models.Abstractions;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Implementations
{
    public class CityService : ICityService
    {
        ICityRepository _cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public List<City> GetAllCities()
        {
            return _cityRepository.Cities.ToList();
        }
    }
}
