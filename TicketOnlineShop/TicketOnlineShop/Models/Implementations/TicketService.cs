﻿using System.Collections.Generic;
using System.Linq;
using TicketOnlineShop.Models.Abstractions;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Implementations
{
    public class TicketService : ITicketService
    {
        private ITicketRepository _ticketRepository;

        public TicketService(ITicketRepository ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }

        public List<Ticket> GetAllTickets()
        {
            return _ticketRepository.Tickets.ToList();
        }

        public List<Ticket> GetTicktesByEventName(string eventName)
        {
            return _ticketRepository.Tickets.Where(e => e.Event.Name == eventName).ToList();
        }
    }
}
