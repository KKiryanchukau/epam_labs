﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketOnlineShop.Models.Abstractions;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Implementations
{
    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public List<Order> GetAllOrders()
        {
            return _orderRepository.Orders.ToList();
        }
    }
}
