﻿using System;
using System.Collections.Generic;
using TicketOnlineShop.Models.Abstractions;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Implementations
{
    class OrderRepository : IOrderRepository
    {
        private List<Order> _orders = new List<Order>()
        {
            new Order
            {
                Buyer = new MyUser { FirstName = "Paul", LastName = "Ince"},
                Status = OrderStatus.WaitingForConfirmation,
                Ticket = new Ticket
                {
                    Event = new Event { Name = "Bestival", Date = DateTime.Parse("25-07-2017"),
                    Venue = new Venue { Address = "Lulworth Estate", City = new City { Name = "Bournemouth" } },
                    PathToBanner = @"\images\Bestival.jpg" },
                    Seller = new MyUser { FirstName = "Daniel", LastName = "Brown" },
                    Price = 23
                }
            },
            new Order
            {
                Buyer = new MyUser { FirstName = "Jorge", LastName = "Davidson"},
                Status = OrderStatus.Confirmed,
                Ticket = new Ticket
                {
                    Event = new Event { Name = "DonauinselfestFestival", Date = DateTime.Parse("05-08-2017"),
                    Venue = new Venue { Address = "Donauinsel", City = new City { Name = "Wien" } },
                    PathToBanner = @"\images\DonauinselfestFestival.jpg" },
                    Seller = new MyUser { FirstName = "Alfie", LastName = "Taylor" },
                    Price = 56
                }
            },
            new Order
            {
                Buyer = new MyUser { FirstName = "Thomas", LastName = "Watson" },
                Status = OrderStatus.Rejected,
                Ticket = new Ticket
                {
                    Event = new Event { Name = "EarthGarden", Date = DateTime.Parse("31-10-2017"),
                    Venue = new Venue { Address = "Ta' Qali National Park", City = new City { Name = "Attard" } },
                    PathToBanner = @"\images\EarthGarden.jpg" },
                    Seller = new MyUser { FirstName = "Thomas", LastName = "Evans" },
                    Price = 45
                }
            },
            new Order
            {
                Buyer = new MyUser { FirstName = "Bart", LastName = "Simppson" },
                Status = OrderStatus.Confirmed,
                Ticket = new Ticket
                {
                    Event = new Event { Name = "Tomorrowland", Date = DateTime.Parse("15.07.2017"),
                    Venue = new Venue { Address = "PRC de Schorre", City = new City { Name = "Boom" } },
                    PathToBanner = @"\images\Tomorrowland.jpg" },
                    Seller = new MyUser { FirstName = "James", LastName = "Williams" },
                    Price = 70
                }
            }
        };

        public IEnumerable<Order> Orders
        {
            get
            {
                return _orders;
            }
        }
    }
}
