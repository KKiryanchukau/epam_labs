﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketOnlineShop.Models.Abstractions;

namespace TicketOnlineShop.Models.Implementations
{
    public class UserService : IUserService
    {
        private UserRepository _userRepository;

        public UserService(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public void CreateTestUsers()
        {
            _userRepository.CreateUsers();
        }
    }
}
