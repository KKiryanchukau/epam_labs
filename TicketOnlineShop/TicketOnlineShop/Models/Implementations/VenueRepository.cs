﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketOnlineShop.Models.Abstractions;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Implementations
{
    public class VenueRepository : IVenueRepository
    {
        private List<Venue> _venues = new List<Venue>()
        {
            new Venue { Name = "Lulworth Estate", Address = "Address in Bournemoth", City = new City { Name = "Bournemouth" } },
            new Venue { Name = "Donauinsel", Address = "Address in Wien", City = new City { Name = "Wien" } },
            new Venue { Name = "Ta' Qali National Park", Address = "Address in Attard", City = new City { Name = "Attard" } },
            new Venue { Name = "PRC de Schorre", Address = "Address in Boom", City = new City { Name = "Boom" } },
            new Venue { Name = "Field Day", Address = "Address in Porto", City = new City { Name = "Porto"} },
            new Venue { Name = "Exit Festival", Address = "Address in Budapest", City = new City { Name = "Budapest"} },
            new Venue { Name = "Dimensions", Address = "Address in Bournemoth", City = new City { Name = "Bournemouth" } },
            new Venue { Name = "Meadows in the Mountains", Address = "Address in Wien", City = new City { Name = "Wien" } },
            new Venue { Name = "Montreux Jazz Festival", Address = "Address in Attard", City = new City { Name = "Attard" } },
            new Venue { Name = "Burning Man", Address = "Address in Boom", City = new City { Name = "Boom" } },
            new Venue { Name = "FYF Fest", Address = "Address in Porto", City = new City { Name = "Porto"} },
            new Venue { Name = "Golden Plains", Address = "Address in Budapest", City = new City { Name = "Budapest"} },
        };

         public IEnumerable<Venue> Venues
        {
            get
            {
                return _venues;
            }
        }
    }
}
