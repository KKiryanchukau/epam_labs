﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface ICityRepository
    {
        IEnumerable<City> Cities { get; }
    }
}
