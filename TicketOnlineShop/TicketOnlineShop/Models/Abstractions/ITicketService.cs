﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface ITicketService
    {
        List<Ticket> GetAllTickets();
        List<Ticket> GetTicktesByEventName(string eventName);
    }
}
