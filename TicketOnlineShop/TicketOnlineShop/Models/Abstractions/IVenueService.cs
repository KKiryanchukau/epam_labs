﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface IVenueService
    {
        List<Venue> GetAllVenues();
    }
}
