﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface IEventRepository
    {
        IEnumerable<Event> Events { get; }
    }
}
