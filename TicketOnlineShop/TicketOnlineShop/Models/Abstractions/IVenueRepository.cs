﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface IVenueRepository
    {
        IEnumerable<Venue> Venues { get; }
    }
}
