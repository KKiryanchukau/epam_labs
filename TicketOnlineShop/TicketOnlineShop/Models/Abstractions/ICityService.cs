﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface ICityService
    {
        List<City> GetAllCities();
    }
}
