﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface IOrderRepository
    {
        IEnumerable<Order> Orders { get; }
    }
}
