﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface ITicketRepository
    {
        IEnumerable<Ticket> Tickets { get; }
    }
}
