﻿using System.Collections.Generic;
using TicketOnlineShop.Models.Entities;

namespace TicketOnlineShop.Models.Abstractions
{
    public interface IOrderService
    {
        List<Order> GetAllOrders();
    }
}
