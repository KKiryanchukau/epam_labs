﻿using System.Security.Claims;

namespace TicketOnlineShop.Models
{
    public class MyUser : ClaimsIdentity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string PathToFoto { get; set; }
        public string FirstNameNormalized => FirstName?.ToUpper();
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public MyRole Role { get; set; }
        public bool TwoFactorEnabled { get; set; }
    }
}