﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Globalization;
using TicketOnlineShop.Models;
using TicketOnlineShop.Models.Abstractions;
using TicketOnlineShop.Models.Implementations;

namespace TicketOnlineShop
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            UserRepository userStore = new UserRepository();
            RoleRepository roleStore = new RoleRepository();

            userStore.CreateUsers();

            services.AddSingleton<IUserStore<MyUser>>(userStore);
            services.AddSingleton<IUserPasswordStore<MyUser>>(userStore);
            services.AddSingleton<IUserEmailStore<MyUser>>(userStore);
            services.AddSingleton<IRoleStore<MyRole>>(roleStore);
            services.AddSingleton<IUserClaimsPrincipalFactory<MyUser>, UserPrincipalFactory>();
            services.AddSingleton<ICityRepository, CityRepository>();
            services.AddSingleton<IVenueRepository, VenueRepository>();
            services.AddSingleton<IEventRepository, EventRepository>();
            services.AddSingleton<ITicketRepository, TicketRepository>();
            services.AddSingleton<IOrderRepository, OrderRepository>();
            services.AddSingleton<ICityService, CityService>();
            services.AddSingleton<IVenueService, VenueService>();
            services.AddSingleton<IEventService, EventService>();
            services.AddSingleton<ITicketService, TicketService>();
            services.AddSingleton<IOrderService, OrderService>();
            services.AddSingleton<IUserService, UserService>();
            
            services.AddIdentity<MyUser, MyRole>();

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc()
                .AddDataAnnotationsLocalization()
                .AddViewLocalization();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en"),
                    new CultureInfo("be"),
                    new CultureInfo("ru")
                };

                options.DefaultRequestCulture = new RequestCulture("en");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            app.UseStaticFiles();
            app.UseIdentity();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"); 
            });
        }
    }
}